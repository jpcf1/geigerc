* DC-DC Boost Converter *

* Simulation parameters
.param VCC=3.98
.param Tsw=200us
.param DutyC=0.75
.param Ton=Tsw*DutyC
.param Rload=4Meg
.param Lval=2.2m
.param Cval=47u
.temp  25

L1   VDD  VLD 'Lval'
D1   VLD  Vo  d1n4007
Co   Vo   0   'Cval'
Ro   Vo	  0	  'Rload'
Rf1  Vo   Vf  0.993Meg
Rf2  Vf   0   4.67k

xqsw VLD  Vg 0 	 IRF840
Rsw		 iclk Vg 470

* The input sources
Viclk  iclk 0 PULSE(0 5 0ns 250ns 250ns 'Ton' 'Tsw')
VDC    VDD  0 DC 'VCC'



* The simulation commands
.tran 500ns 60s
*.plot tran V(Vo)
*.step param Cval list 10u 47u 100u 220u 470u
*.step param DutyC list 0.7 0.8 0.9

* Useful measurements
.meas TRAN Pout  	 AVG -I(Ro)*V(Vo)
.meas TRAN Pin   	 AVG I(VDC)*V(VDD)
.meas TRAN IDavg  	 AVG I(D1)
.meas TRAN Iavg  	 AVG I(L1)
.meas TRAN Ipeak 	 MAX I(L1)
.meas TRAN ICap		 AVG I(Co)
.meas Effc	param	 Pout/Pin
.meas TRAN Vo_max	 MAX V(Vo)
.meas TRAN Vf_max    MAX V(Vf)

***** THE COMPONENT MODELS *****
.SUBCKT IRF840 1 2 3
M1 9 7 8 8 MM L=100u W=100u
.MODEL MM NMOS LEVEL=1 IS=1e-32
+VTO=3.84925 LAMBDA=0.00279225 KP=6.49028
+CGSO=1.18936e-05 CGDO=1e-11
RS 8 3 0.0178672
D1 3 1 MD
.MODEL MD D IS=6.51041e-09 RS=0.0106265 N=1.49911 BV=500
+IBV=0.00025 EG=1.2 XTI=3.02565 TT=0.0001
+CJO=1.08072e-09 VJ=3.67483 M=0.9 FC=0.5
RDS 3 1 2e+07
RD 9 1 0.810848
RG 2 7 3.45326
D2 4 5 MD1
.MODEL MD1 D IS=1e-32 N=50
+CJO=1.81945e-09 VJ=1.07167 M=0.9 FC=1e-08
D3 0 5 MD2
.MODEL MD2 D IS=1e-10 N=1 RS=3e-06
RL 5 10 1
FI2 7 9 VFI2 -1
VFI2 4 0 0
EV16 10 0 9 7 1
CAP 11 10 1.81945e-09
FI1 7 9 VFI1 -1
VFI1 11 6 0
RCAP 6 10 1
D4 0 6 MD3
.MODEL MD3 D IS=1e-10 N=1
.ENDS

.MODEL D1N4007 d (
+IS=3.19863e-08 RS=0.0428545 N=2 EG=0.784214
+XTI=0.504749 BV=1100 IBV=0.0001 CJO=4.67478e-11
+VJ=0.4 M=0.469447 FC=0.5 TT=8.86839e-06
+KF=0 AF=1 )

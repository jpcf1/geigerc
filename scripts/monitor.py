import serial
import matplotlib.pyplot as plt
import numpy as np

# Conversion constants
VOLTAGE_ENDOFSCALE = 5.0
ADC_ENDOFSCALE     = int('3FF', base=16)
ADC_CONVERSION     = 0.00468
SAMPLING_T         = 0.02

# Creating the Plot
t = np.arange(0, 25, SAMPLING_T)
NUM_SAMPLES = t.size

va = np.zeros(NUM_SAMPLES)
da = np.zeros(NUM_SAMPLES)

fig = plt.figure()

with serial.Serial('/dev/ttyS4', 115200) as serfd:
    for i in range(NUM_SAMPLES):
        v,d = serfd.readline().strip().split(b',')
        v = int(v.decode('ascii'), base=16)
        d = int(d.decode('ascii'), base=16)
        va[i] = (VOLTAGE_ENDOFSCALE * (v/ADC_ENDOFSCALE)) / ADC_CONVERSION
        da[i] = d
        #d = int(d, base=16)
        #print(str(line, 'ascii'))
        #print("Voltage: ", v, "-- PWM: ", d)

plt.plot(t, va)
plt.grid(True)
plt.show()

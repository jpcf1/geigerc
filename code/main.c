#include <PIC16F1825.h>
#include <xc.h>
#include <stdint.h>
#include <stdio.h>

#include "pic16f1825_configs.h"
#include "lib/pic16f1825_utils/include/pic16f1825_utils.h"
#include "lib/uart/include/uart_lib.h"

//#define UART_FLOAT_DEBUG 0
#define UART_CHAR_DEBUG  1

void interrupt isr() {
}

void main(void) {
    
    uint16_t adc_sample;
    uint16_t pwm_dutyc;

#if (defined UART_FLOAT_DEBUG) || (defined UART_CHAR_DEBUG)
    char adc_sample_s[32];
    float voltage;
#endif
    
    // Sets clock to internal generator @ 16MHz
    pic16f1825_init();
    
    // Configure IO pin direction (as outputs)
    TRISC = 0x00;
    
    // Configures the PWM Operation on pin RC3
    pic16f1825_PWM_init(PWM_DUTYC_MAX);
    
    // Configures the ADC Operation on pin RC2
    pic16f1825_ADC_init();
    
    // Configures the UART Operation on pin RC4
#if (defined UART_FLOAT_DEBUG) || (defined UART_CHAR_DEBUG)
    UART_init();
#endif
    
    // During the first second, keep PWM Duty Cycle at minimum
    pwm_dutyc = PWM_DUTYC_MAX;
    pic16f1825_PWM_setDutyCycle(pwm_dutyc);
    //__delay_ms(1000);
    
    
    
    while(1) {
        __delay_ms(DCDC_UPDATE_TIME_MS);
        
        // DCDC Controller
        adc_sample = pic16f1825_ADC_sample();

        if(adc_sample < ADC_TARGET_V) {
            if(pwm_dutyc < PWM_DUTYC_MAX)
                pwm_dutyc += 0x001;
            else
                pwm_dutyc = PWM_DUTYC_MAX;
        }
        else {
            if(pwm_dutyc > PWM_DUTYC_MIN)
                pwm_dutyc -= 0x001;
            else
                pwm_dutyc = PWM_DUTYC_MIN;
        }
        
        pic16f1825_PWM_setDutyCycle(pwm_dutyc);
        
#ifdef UART_FLOAT_DEBUG
        voltage = VOLTAGE_ENDOFSCALE * (float)adc_sample / ADC_ENDOFSCALE;
        sprintf(adc_sample_s, "%6.4fV -- %03X\n\r ", voltage, pwm_dutyc);
        UART_transmit(adc_sample_s, 19);
#endif
#ifdef UART_CHAR_DEBUG
        sprintf(adc_sample_s, "%03X,%03X\n", adc_sample, pwm_dutyc);
        UART_transmit(adc_sample_s, 8);
#endif
    }
    
    return;
}
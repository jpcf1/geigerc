/* Utility functions for the PIC16F1825 setup and configuration */

#include "../../../pic16f1825_configs.h"

// Useful flags
#define ENABLED  1
#define DISABLED 0

// The ADC, 10-bit End-of-Scale value
#define ADC_ENDOFSCALE 0x3FF

void pic16f1825_init(void);
void pic16f1825_TMR2_init(void);
void pic16f1825_PWM_setDutyCycle(uint16_t dutyc_set);
void pic16f1825_PWM_init(uint16_t dutyc_init);
void pic16f1825_PWM_setPeriod(void);
void pic16f1825_ADC_init(void);
uint16_t pic16f1825_ADC_sample(void);

/* Utility functions for the PIC16F1825 setup and configuration */

#include <pic16f1825.h>

#include "../include/pic16f1825_utils.h"

void pic16f1825_init(void){
    /* 16MHz clock */
    OSCCONbits.SCS = 0;       /* clear SCS bits <1:0> */ 
    OSCCONbits.IRCF = 0b1111; /* Set IRCF<3:0> to 1111 ( bits 3-6 of OSCCON), Internal Oscillator running at 16MHz */
    OSCTUNE = 0x06;           /* Tunes the PLL for better accuracy */
    
    /* wait for clock to get stable */
    while(OSCSTATbits.HFIOFR == 0 || OSCSTATbits.HFIOFS == 0); 
}

void pic16f1825_TMR2_init(void) {
    
    // Configures and activates interrupts
    INTCONbits.GIE  = 0; // Globally disables interrupts
    INTCONbits.PEIE = 0; // Disables peripheral interrupts, where TMR2 is located
    PIE1bits.TMR2IE = 0; // Disables interrupts for TMR2
    
    // Configures timer 2
    TMR2  = 0x00;
    PR2   = 249; // With 1MHz clock and 1:64 & 1:16, this gives an interrupt each second
    T2CON = 0x7C; // Prescaler to 1 and Postscaler to 16
    T2CONbits.TMR2ON = 1; // Turns on the Timer
}

void pic16f1825_TMR1_init(void) {
    
    // Configures and activates interrupts
    INTCONbits.GIE  = 1; // Globally activates interrupts
    INTCONbits.PEIE = 1; // Enables peripheral interrupts, where TMR2 is located
    PIE1bits.TMR1IE = 1; // Enables interrupts for TMR1
    
    // Configures timer 1
    T1CONbits.T1CKPS  = 0x0;  // Prescaler value to 1
    T1CONbits.TMR1CS  = 0b10; // Clock source is set to external oscilator
    T1CONbits.T1OSCEN = 1;    // Enables the external oscillator
    TMR1H = 0x80;
    TMR1L = 0x00;
    T1CONbits.TMR1ON = 1;
    
    /* ALTERNATIVE: Source is External oscillator
    // Configures timer 1
    T1CONbits.T1CKPS = 0x0; // Prescaler value to 1
    T1CONbits.TMR1CS = 0x0; // Clock source is set to Fosc/4
    TMR1H = 0x63;
    TMR1L = 0xC0;
    T1CONbits.TMR1ON = 1;
     */
}

void pic16f1825_PWM_setDutyCycle(uint16_t dutyc_set) {
    CCPR2L           = (dutyc_set >> 2) & 0xFF;  // Set duty cycle 10 MSBs from dutyc_set
    CCP2CONbits.DC2B = dutyc_set & 0x03;          // Set duty cycle  2 MSBs from dutyc_set
}


void pic16f1825_PWM_init(uint16_t dutyc_init) {
    
    // Disables the output driver in the RC3/CCP2 pin. This prevents the pin to be 
    // mistakenly driven during configuration
    TRISCbits.TRISC3 = 1;
    
    // Loading the PR2 Register -> configures TMR2 for 200us period
    TMR2 = 0x00;                     // Pre-load set to 0
    T2CONbits.T2CKPS = 0b01;         // Pre-scaler set to 1:4
    PR2  = 200;                      // 200uS period @16MHz && 1:4. 1us resolution
    
    CCP2CONbits.CCP2M = 0b1100;      // Enable normal PWM mode
    
    // Sets the initial duty cycle
    CCPR2L           = 0x96;
    CCP2CONbits.DC2B = 0x3;
    //pic16f1825_PWM_setDutyCycle(dutyc_init);
    
    CCPTMRSbits.C2TSEL = 0x00;       // CCP2 uses TMR2. Otherwise, 1->TMR4, 2->TMR6
    PIR1bits.TMR2IF = 0;             // Clear TMR2 Interrupt Flag
    
    T2CONbits.TMR2ON = 1;            // Enable TMR2
    
    // TODO Wait for a full cycle of TMR2. Optional, so that we start with a full PWM cycle
    
    // Re-enable the output driver of CCP2
    TRISCbits.TRISC3 = 0;
}

void pic16f1825_PWM_setPeriod(void) {
    
}

void pic16f1825_ADC_init(void) {
    
    // Disable output CMOS driver, and configure it as Analog input pin
    TRISCbits.TRISC2 = 1;
    ANSELCbits.ANSC2 = 1;
    
    // Selects AN6/RC2/Pin8 as the signal to be sampled
    ADCON0bits.CHS = 6;

    ADCON1bits.ADCS = 0x4;     // Conversion clock set to Fosc/4
    ADCON1bits.ADPREF = 0x00;  // Positive reference connected to VDD
    ADCON1bits.ADNREF = 0x00;  // Positive reference connected to VDD
    ADCON1bits.ADFM   = 1;     // Right-justified result
    ADCON0bits.ADON   = 1;     // Enables ADC
}

uint16_t pic16f1825_ADC_sample(void) {
    
    uint16_t conv_result;
    
    // Start Conversion
    ADCON0bits.ADGO = 1;
    
    // Wait for conversion to be ready
    while(ADCON0bits.ADGO == 1);
    
    // Store sampled value
    conv_result = (ADRESL | (ADRESH << 8)) & 0x3FF;
    
    return conv_result;
}